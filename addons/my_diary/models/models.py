# -*- coding: utf-8 -*-

from odoo import models, fields, api


class my_diary(models.Model):
    _name = 'my_diary.my_diary'
    _description = 'My Diary Task'

    timestamp = fields.Date()
    diary_entry = fields.Text(required=True)
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         for record in self:
#             record.value2 = float(record.value) / 100

